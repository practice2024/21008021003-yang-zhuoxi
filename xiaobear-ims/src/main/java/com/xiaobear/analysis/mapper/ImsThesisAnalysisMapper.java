package com.xiaobear.analysis.mapper;

import com.xiaobear.analysis.domain.ImsThesisStatisticsVo;

import java.util.List;

/**
 * @author xiaobear
 */
public interface ImsThesisAnalysisMapper {

    /**
     * 论文统计分析
     * @param vo
     * @return
     */
    public List<ImsThesisStatisticsVo> selectThesisStatisticsList(ImsThesisStatisticsVo vo);
}
