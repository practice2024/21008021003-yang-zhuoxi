package com.xiaobear.common.enums;

/**
 * 操作状态
 * 
 * @author xiaobear
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
