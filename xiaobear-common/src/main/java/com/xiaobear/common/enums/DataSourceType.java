package com.xiaobear.common.enums;

/**
 * 数据源
 * 
 * @author xiaobear
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
